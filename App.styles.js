/**
 * Created by brunopoeta on 23/08/17.
 */

import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        flex: 1,
        padding: 15,
        paddingTop: 30
    },
    spinner: {
        alignItems: 'center',
        bottom: 0,
        justifyContent: 'center',
        left: 0,
        position: 'absolute',
        right: 0,
        top: 0
    },
    spinnerBackdrop: {
        backgroundColor: '#000',
        bottom: 0,
        left: 0,
        opacity: .7,
        position: 'absolute',
        right: 0,
        top: 0

    }
});