import React from 'react';
import {
    Alert,
    ActivityIndicator,
    StyleSheet,
    Text,
    View
} from 'react-native';

// Components
import ActionButtons from './app/components/ActionButtons/ActionButtons';
import Calendar from './app/components/Calendar/Calendar';
import UserPicker from './app/components/UserPicker/UserPicker';

// Server defaults
import './app/config/server';

// Store
import {store} from './app/store';

// Styles
import {styles} from './App.styles';

export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    componentWillMount() {
        store.subscribe(() => {
            const storeState = store.getState();

            this.setState({
                isFetchingUsers: storeState.userReducer.isFetchingUsers,
                isSendingStatus: storeState.timesheetReducer.isSendingStatus,
                timesheetSent: storeState.timesheetReducer.timesheetSent
            });

            // In case timesheet status (approve/reject) is sent
            // give user visual feedback
            if (this.state.timesheetSent) {
                this.alertUser();
            }
        });
    }

    alertUser() {
        const success = this.state.timesheetSent.success;
        const status = this.state.timesheetSent.status;

        Alert.alert(
            (success) ? 'Success' : 'Problem occurred',
            (success) ? `The selected week was ${(status === 'approve') ? 'approved' : 'rejected'} successfully!` : `An error has occurred while trying to ${status} selected week.`,
            [
                {
                    text: 'OK'
                }
            ],
            {cancelable: false}
        );

        // Clear status so when store is changed
        // won't fire the alert again
        store.dispatch({
            type: 'CLEAR TIMESHEET STATUS'
        });
    }

    renderLoading() {
        if (this.state.isFetchingUsers || this.state.isSendingStatus) {
            return (
                <View style={styles.spinner}>
                    <View style={styles.spinnerBackdrop}/>
                    <ActivityIndicator
                        size="large"/>
                </View>
            );
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <UserPicker />
                <Calendar />
                <ActionButtons/>
                {this.renderLoading()}
            </View>
        );
    }
}
