/**
 * Created by brunopoeta on 17/08/17.
 */

// Reducers
import {calendarReducer} from '../reducers/calendar';
import {timesheetReducer} from '../reducers/timesheet';
import {userReducer} from '../reducers/user';

// Vendors
import {
    applyMiddleware,
    combineReducers,
    createStore
} from 'redux';
import thunk from 'redux-thunk';

// Redux middlewares
const middleware = applyMiddleware(thunk);

// Combine reducers
const reducers = combineReducers({calendarReducer, timesheetReducer, userReducer});

// Export store
export const store = createStore(reducers, middleware);
