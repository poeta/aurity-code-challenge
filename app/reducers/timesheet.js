/**
 * Created by brunopoeta on 22/08/17.
 */

export const timesheetReducer = (state = {}, action) => {
    switch (action.type) {
        case 'CLEAR TIMESHEET STATUS':
            return Object.assign({}, state, {timesheetSent: null});

            break;

        case 'SENDING TIMESHEET STATUS':
            return Object.assign({}, state, {isSendingStatus: true});

            break;

        case 'SEND TIMESHEET STATUS ERROR':
            return Object.assign({}, state, {
                isSendingStatus: false,
                timesheetSent: {
                    success: false,
                    status: action.payload
                }
            });

            break;

        case 'SEND TIMESHEET STATUS SUCCESS':
            return Object.assign({}, state, {
                isSendingStatus: false,
                timesheetSent: {
                    success: (typeof action.payload.approved_by_id != 'undefined'),
                    status: action.payload.status
                }
            });

            break;

        default:
            return state;
    }
};