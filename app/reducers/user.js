/**
 * Created by brunopoeta on 17/08/17.
 */

export const userReducer = (state = {}, action) => {
    switch (action.type) {
        case 'CHANGE USER':
            return Object.assign({}, state, {user: action.payload});
            break;

        case 'FETCHING USERS':
            return Object.assign({}, state, {isFetchingUsers: true});
            break;

        case 'FETCH USERS SUCCESS':
            return Object.assign({}, state, {isFetchingUsers: false, users: action.payload, error: false});
            break;

        case 'FETCHING USER TIMESHEET':
            return Object.assign({}, state, {isFetchingTimesheet: true});
            break;

        case 'FETCH USER TIMESHEET SUCCESS':
            return Object.assign({}, state, {isFetchingTimesheet: false, timesheet: action.payload, error: false});
            break;

        default:
            return state;
    }
};