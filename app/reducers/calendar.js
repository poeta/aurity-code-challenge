/**
 * Created by brunopoeta on 22/08/17.
 */

const defaultState = {
    month: new Date().getMonth() + 1
};

export const calendarReducer = (state = defaultState, action) => {
    switch (action.type) {
        case 'CHANGE MONTH':
            const month = (action.payload === 'inc') ? state.month + 1 : state.month - 1;
            return Object.assign({}, state, {month});

            break;

        case 'CHANGE WEEK':
            return Object.assign({}, state, {
                week: {
                    id: (action.payload.id) ? action.payload.id : null,
                    index: action.payload.index
                }
            });

            break;

        default:
            return state;
    }
};