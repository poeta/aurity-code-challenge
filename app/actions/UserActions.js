/**
 * Created by brunopoeta on 17/08/17.
 */

import {Alert} from 'react-native';

// Vendors
import axios from 'axios';

const getUsers = () => {
    return (dispatch) => {
        dispatch({
            type: 'FETCHING USERS'
        });

        return axios({
            url: '/users',
            timeout: 20000,
            method: 'get',
            responseType: 'json'
        }).then((response) => {
            dispatch({
                payload: response.data,
                type: 'FETCH USERS SUCCESS'
            });

        }).catch(() => {
            Alert.alert(
                'Problem occurred',
                'An error has occurred while trying to fetch users. Try again?',
                [
                    {
                        onPress: () => {
                            getUsers();
                        },
                        text: 'OK'
                    }
                ],
                {cancelable: false}
            );
        });
    }
};

// Export actions
export {getUsers};