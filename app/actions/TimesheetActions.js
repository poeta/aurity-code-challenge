/**
 * Created by brunopoeta on 22/08/17.
 */

import axios from 'axios';

const getUserTimesheet = (user, month) => {
    return (dispatch) => {
        dispatch({
            type: 'FETCHING USER TIMESHEET'
        });

        return axios({
            url: `/training/weeks/${month}/2017/${user.id}`,
            timeout: 20000,
            method: 'get',
            responseType: 'json'
        }).then((response) => {
            dispatch({
                payload: response.data.data,
                type: 'FETCH USER TIMESHEET SUCCESS'
            });
        }).catch((err) => {
            console.log(err);
        });
    }
};

const sendTimesheetStatus = (data) => {
    return (dispatch) => {
        dispatch({
            type: 'SENDING TIMESHEET STATUS'
        });

        return axios({
            url: `/training/weeks/${data.week.id}/users/${data.user.id}`,
            timeout: 20000,
            method: 'put',
            responseType: 'json',
            params: {
                status: data.status
            }
        }).then((response) => {
            dispatch({
                payload: response.data,
                type: 'SEND TIMESHEET STATUS SUCCESS'
            });
        }).catch(() => {
            dispatch({
                payload: data.status,
                type: 'SEND TIMESHEET STATUS ERROR'
            });
        });
    }
};

// Export actions
export {getUserTimesheet, sendTimesheetStatus};