/**
 * Created by brunopoeta on 22/08/17.
 */

import {
    Dimensions,
    StyleSheet
} from 'react-native';

export const styles = StyleSheet.create({
    actionButton: {
        alignItems: 'center',
        alignSelf: 'stretch',
        backgroundColor: '#CCC',
        borderRadius: 4,
        flex: (Dimensions.get('window').width > 320) ? null : 1,
        marginBottom: 5,
        marginLeft: (Dimensions.get('window').width > 320) ? null : 5,
        marginRight: (Dimensions.get('window').width > 320) ? null : 5,
        marginTop: 5,
        padding: 15
    },
    actionButton__approve: {
        backgroundColor: '#53CA58'
    },
    actionButton__reject: {
        backgroundColor: '#FF5151'
    },
    actionButtonText: {
        backgroundColor: 'transparent',
        color: '#FFF',
        fontWeight: 'bold'
    },
    actionButtons: {
        alignItems: 'center',
        flexDirection: (Dimensions.get('window').width > 320) ? 'column' : 'row',
        justifyContent: 'flex-end',
        marginBottom: -5,
        marginLeft: (Dimensions.get('window').width > 320) ? null : -5,
        marginRight: (Dimensions.get('window').width > 320) ? null : -5,
        marginTop: -5
    },
    message: {
        backgroundColor: '#CCC',
        borderRadius: 4,
        padding: 10,
    },
    messageText: {
        textAlign: 'center'
    }
});