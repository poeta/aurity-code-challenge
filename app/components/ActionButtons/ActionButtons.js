/**
 * Created by brunopoeta on 22/08/17.
 */

import React from 'react';
import {
    Alert,
    Text,
    TouchableOpacity,
    View
} from 'react-native';

// Actions
import {sendTimesheetStatus} from '../../actions/TimesheetActions';

// Store
import {store} from '../../store';

// Styles
import {styles} from './styles';

export default class ActionButtons extends React.Component {

    // Life-cycle functions

    constructor(props) {
        super(props);

        this.state = {
            week: null
        };
    }

    componentWillMount() {
        store.subscribe(() => {
            const storeState = store.getState();

            this.setState({
                user: storeState.userReducer.user,
                timesheet: storeState.userReducer.timesheet,
                week: storeState.calendarReducer.week
            });
        });
    }

    // Component functions

    handleButtonPress(context) {
        // Check if context is approve or reject
        // and give user feedback. Also check if
        // user is sure about his/her decision.
        Alert.alert(
            (context === 'approve') ? 'Approve week' : 'Reject week',
            `Are you sure you want to ${context} this user's work week?`,
            [
                {
                    onPress: () => this.sendTimesheetStatus(context),
                    text: 'Yes'
                },
                {
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                    text: 'No'
                }
            ],
            {cancelable: false}
        )
    }

    sendTimesheetStatus(context) {
        // Send timesheet status/feedback
        // to api
        store
            .dispatch(sendTimesheetStatus({
                status: context,
                user: this.state.user,
                week: this.state.week
            }));
    }

    // Render functions

    renderButtons() {
        if (this.state.timesheet && this.state.week) {
            return (
                <View style={styles.actionButtons}>

                    <TouchableOpacity
                        activeOpacity={.8}
                        onPress={() => {
                            this.handleButtonPress('approve')
                        }}
                        style={[styles.actionButton, styles.actionButton__approve]}>

                        <Text style={styles.actionButtonText}>
                            Approve
                        </Text>

                    </TouchableOpacity>

                    <TouchableOpacity
                        activeOpacity={.8}
                        onPress={() => {
                            this.handleButtonPress('reject')
                        }}
                        style={[styles.actionButton, styles.actionButton__reject]}>

                        <Text style={styles.actionButtonText}>
                            Reject
                        </Text>

                    </TouchableOpacity>
                </View>
            );
        }
    }

    renderMessage() {
        if (!this.state.timesheet || !this.state.week) {
            return (
                <View style={styles.message}>
                    <Text style={styles.messageText}>
                        Please select a user and a week to accept or reject.
                    </Text>
                </View>
            );
        }
    }

    render() {
        return (
            <View>
                {this.renderMessage()}
                {this.renderButtons()}
            </View>
        );
    }
}