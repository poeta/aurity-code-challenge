/**
 * Created by brunopoeta on 21/08/17.
 */

import {
    Dimensions,
    StyleSheet
} from 'react-native';

export const styles = StyleSheet.create({
    picker: {
        color: '#666',
        fontSize: (Dimensions.get('window').width > 320) ? 14 : 12,
        height: (Dimensions.get('window').width > 320) ? 42 : 32,
        paddingLeft: 5,
        paddingRight: 5,
    },
    pickerLabel: {
        color: '#666',
        fontSize: (Dimensions.get('window').width > 320) ? 15 : 13,
        marginBottom: (Dimensions.get('window').width > 320) ? 10 : 7
    },
    pickerLoader: {
        color: '#666',
        marginBottom: 20,
        textAlign: 'center'
    },
    pickerWrapper: {
        borderColor: '#CCC',
        borderWidth: 1,
        borderRadius: 2,
        marginBottom: (Dimensions.get('window').width > 320) ? 20 : 10
    }
});
