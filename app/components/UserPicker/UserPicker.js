/**
 * Created by brunopoeta on 21/08/17.
 */

import React from 'react';
import {
    Text,
    View
} from 'react-native';

// Actions
import {getUsers} from '../../actions/UserActions';
import {getUserTimesheet} from '../../actions/TimesheetActions';

// Store
import {store} from '../../store/index';

// Styles
import {styles} from './styles';

// Vendors
import {Picker} from 'react-native-picker-dropdown';

export default class UserPicker extends React.Component {
    
    // Life-cycle functions
    
    constructor(props) {
        super(props);

        this.state = {
            isFetchingTimesheet: false,
            selectedUser: null
        };
    }

    componentWillMount() {
        store.subscribe(() => {
            const storeState = store.getState();

            this.setState({
                isFetchingUsers: storeState.userReducer.isFetchingUsers,
                isFetchingTimesheet: storeState.userReducer.isFetchingTimesheet,
                month: storeState.calendarReducer.month,
                timesheet: storeState.userReducer.timesheet,
                selectedUser: storeState.userReducer.user,
                users: storeState.userReducer.users
            });
        });

        // Fetch users before component
        // is mounted
        this.getUsers();
    }
    
    // Component functions

    changeUser(user) {
        store.dispatch({
            payload: user,
            type: 'CHANGE USER'
        });

        // Fetch user timesheet of current month
        // whenever user is changed
        this.getUserTimesheet(user);
    }

    getUserTimesheet(user) {
        store.dispatch(getUserTimesheet(user, this.state.month));
    }

    getUsers() {
        // Dispatch asynchronous function
        // getUsers to fetch users from api
        store.dispatch(getUsers());
    }
    
    // Render functions

    renderLoading() {
        if (this.state.isFetchingUsers) {
            return (
                <Text style={styles.pickerLoader}>
                    Loading users...
                </Text>
            )
        }
    }

    renderUserPicker() {
        if (this.state.users) {
            return (
                <View>
                    <Text style={styles.pickerLabel}>
                        Select user
                    </Text>

                    <View style={styles.pickerWrapper}>
                        <Picker
                            selectedValue={this.state.selectedUser}
                            onValueChange={(user) => {
                                this.changeUser(user)
                            }}
                            style={styles.picker}>
                            {this.renderUsers()}
                        </Picker>
                    </View>
                </View>
            );
        }
    }

    renderUsers() {
        return this
            .state
            .users
            .map((user, index) => {
                return (
                    <Picker.Item
                        key={index}
                        label={`${user.username} (${user.email})`}
                        value={user}/>
                )
            });
    }

    render() {
        return (
            <View>
                {this.renderLoading()}
                {this.renderUserPicker()}
            </View>
        );

    }
}