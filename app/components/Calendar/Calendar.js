/**
 * Created by brunopoeta on 17/08/17.
 */

import React from 'react';
import {
    ActivityIndicator,
    Button,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native';

// Actions
import {getUserTimesheet} from '../../actions/TimesheetActions';

// Store
import {store} from '../../store/index';

// Styles
import {styles} from './styles';

// Vendors
import _ from 'lodash';

export default class Calendar extends React.Component {

    // Life-cycle functions

    constructor(props) {
        super(props);

        const currentDate = new Date();

        this.state = {
            days: [],
            isFetchingTimesheet: false,
            month: currentDate.getMonth() + 1,
            year: currentDate.getFullYear()
        };
    }

    componentWillMount() {
        store.subscribe(() => {
            const storeState = store.getState();

            this.setState({
                isFetchingTimesheet: storeState.userReducer.isFetchingTimesheet,
                isFetchingUsers: storeState.userReducer.isFetchingUsers,
                month: storeState.calendarReducer.month,
                timesheet: storeState.userReducer.timesheet,
                user: storeState.userReducer.user,
                week: storeState.calendarReducer.week
            });

            if (this.state.timesheet) {
                this.convertTimesheet();
            }
        });
    }

    // Component functions

    changeWeek(index, id) {
        store.dispatch({
            payload: {
                id,
                index
            },
            type: 'CHANGE WEEK'
        });
    }

    changeMonth(op) {
        store.dispatch({
            payload: op,
            type: 'CHANGE MONTH'
        });

        if (this.state.user) {
            // Whenever user changes month
            // fetch timesheet for that month
            store.dispatch(getUserTimesheet(this.state.user, this.state.month));
        }
    }

    convertTimesheet() {
        const weeks = [];

        // Get all days
        _.each(this.state.timesheet.weeks, (week) => {
            weeks.push(week.days_in_week);
        });

        // And flatten then into a single-level array
        // so it's easier to lay them on the calendar
        this.setState({
            timesheetDays: _.flatten(_.union(weeks))
        });
    }

    getAdjacentMonthDays(context) {
        const days = [];
        const firstDay = new Date(this.state.year, this.state.month - 1, 1).getDay() - 1;
        const lastDay = new Date(this.state.year, this.state.month - 1, this.getDaysInMonth()).getDay() - 1;
        const previousMonthDays = this.getDaysInMonth(this.state.month - 1);

        // Get previous or next month days
        // that will appear on the calendar
        if (context === 'previous') {
            for (let i = 0; i < (firstDay > -1 ? firstDay : 6); i++) {
                let day = {
                    disabled: true,
                    number: previousMonthDays - i
                };

                days.push(day);
            }

            return days.reverse();
        } else {
            for (let i = 0; i < (6 - (lastDay > -1 ? lastDay : 6)); i++) {
                days.push({
                    disabled: true,
                    number: i + 1
                });
            }

            return days;
        }
    }

    getCalendarDays() {
        const previousDays = this.getAdjacentMonthDays('previous');
        const monthDays = this.getDaysInMonth();
        const monthDaysArray = Array.from({length: monthDays}, (v, k) => {
            return {number: k + 1};
        });
        const nextDays = this.getAdjacentMonthDays('next');

        // Concatenating previous, current and next
        // month's days to correctly display current
        // month calendar
        return previousDays.concat(monthDaysArray).concat(nextDays);
    }

    getDaysInMonth(month) {
        const days = [31, (this.isLeapYear() ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // Return the number of days in month.
        // If function parameter month is not defined
        // it will use this.state.month as default
        if (month || this.state.month) {
            return days[(month || this.state.month) - 1];
        }
    }

    getWeekDays(days) {
        const weekArray = [];

        // Divide days array into
        // 7-items arrays, that will be
        // rendered as rows
        for (let i = 0; i < days.length; i++) {
            const week = Math.floor(i / 7);

            if (!weekArray[week]) {
                weekArray[week] = [];
            }

            weekArray[week].push(days[i]);
        }

        return weekArray;
    }

    getWeekId(days) {
        // Double iteration to get week id
        // based on day numbers on array days
        if (this.state.timesheet) {
            for (let i = 0; i < days.length; i++) {
                let day = days[i];

                for (let j = 0; j < this.state.timesheet.weeks.length; j++) {
                    let week = this.state.timesheet.weeks[j];

                    if (_.find(week.days_in_week, {day_number: day.number})) {
                        return week.week_id;
                    }
                }
            }
        } else {
            return null;
        }
    }

    isLeapYear() {
        return new Date(this.state.year, 1, 29).getDate() === 29;
    }

    // Render functions

    renderDayHours(day) {
        if (this.state.timesheetDays && !day.disabled) {
            const timesheetDay = _.find(this.state.timesheetDays, {day_number: day.number});
            const hours = (timesheetDay) ? `${timesheetDay.hours}h${(timesheetDay.minutes) ? timesheetDay.minutes : ''}` : null;

            if (hours) {
                return (
                    <View style={styles.dayHours}>
                        <Text style={styles.dayHoursText}>
                            {hours}
                        </Text>
                    </View>
                );
            }
        }
    }

    renderDays(days) {
        return days
            .map((day, index) => {
                return (
                    <View key={index}
                          style={styles.day}>
                        <Text style={styles.dayText}>
                            {day.number}
                        </Text>

                        {this.renderDayHours(day)}
                    </View>
                )
            });
    }

    renderMonth() {
        const monthArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        return monthArray[this.state.month - 1];
    }

    renderMonthSelector() {
        return (
            <View style={styles.monthSelector}>
                {this.renderMonthSelectorButton('dec')}
                <Text style={styles.month}>
                    {this.renderMonth()}
                </Text>
                {this.renderMonthSelectorButton('inc')}
            </View>
        );
    }

    renderMonthSelectorButton(op) {
        if (op === 'dec' && this.state.month > 1) {
            return (
                <TouchableOpacity activeOpacity={.7}
                                  onPress={() => {
                                      this.changeMonth(op)
                                  }}
                                  style={styles.monthSelectorButton}>
                    <Text>&lt;</Text>
                </TouchableOpacity>
            );
        } else if (op === 'inc' && this.state.month < 12) {
            return (
                <TouchableOpacity activeOpacity={.7}
                                  onPress={() => {
                                      this.changeMonth(op)
                                  }}
                                  style={styles.monthSelectorButton}>
                    <Text>&gt;</Text>
                </TouchableOpacity>
            );
        }
    }

    renderTimesheetLoading() {
        if (this.state.isFetchingTimesheet) {
            return (
                <ActivityIndicator
                    style={styles.spinner}
                    size="large"/>
            );
        }
    }

    renderWeekDays() {
        const daysNames = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];

        return daysNames
            .map((name, index) => {
                return (
                    <Text key={index}
                          style={styles.weekDay}>{name.toUpperCase()}</Text>
                )
            });
    }

    renderWeeks() {
        return this
            .getWeekDays(this.getCalendarDays())
            .map((weekDays, index) => {

                return (
                    <TouchableOpacity key={index}
                                      activeOpacity={.8}
                                      onPress={() => {
                                          this.changeWeek(index, this.getWeekId(weekDays))
                                      }}>
                        <View
                            style={[styles.week, (this.state.isFetchingTimesheet ? styles.week__disabled : null), (this.state.week && this.state.week.index === index ? styles.week__selected : null)]}>
                            {this.renderDays(weekDays)}
                        </View>
                    </TouchableOpacity>
                );

            });
    }

    render() {
        if (!this.state.isFetchingUsers) {
            return (
                <View style={styles.container}>

                    {this.renderMonthSelector()}

                    <View style={styles.weekDays}>
                        {this.renderWeekDays()}
                    </View>

                    <View>
                        {this.renderWeeks()}
                        {this.renderTimesheetLoading()}
                    </View>

                </View>
            );
        } else {
            return null;
        }
    }
}