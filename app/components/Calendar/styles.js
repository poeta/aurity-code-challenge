/**
 * Created by brunopoeta on 23/08/17.
 */

import {
    Dimensions,
    StyleSheet
} from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    day: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
        margin: 2,
        padding: (Dimensions.get('window').width > 320) ? 7 : 5
    },
    day__inactive: {
        opacity: .7
    },
    dayHoursText: {
        backgroundColor: 'transparent',
        color: '#999',
        fontSize: 11,
        marginTop: 2
    },
    dayText: {
        color: '#666',
        fontSize: (Dimensions.get('window').width > 320) ? 17 : 15
    },
    month: {
        alignSelf: 'center',
        color: '#666',
        flex: 1,
        fontSize: (Dimensions.get('window').width > 320) ? 17 : 15,
        textAlign: 'center'
    },
    monthSelector: {
        flexDirection: 'row',
        marginBottom: 20
    },
    monthSelectorButton: {
        borderColor: '#CCC',
        borderRadius: 2,
        borderWidth: 1,
        padding: 7
    },
    spinner: {
        alignItems: 'center',
        bottom: 0,
        justifyContent: 'center',
        left: 0,
        position: 'absolute',
        right: 0,
        top: 0
    },
    week: {
        borderColor: '#FFF',
        borderRadius: 4,
        borderWidth: 2,
        flexDirection: 'row'
    },
    week__disabled: {
        opacity: .5
    },
    week__selected: {
        borderColor: '#EDCB46',
        borderRadius: 4,
        borderWidth: 2
    },
    weekDay: {
        color: '#999',
        fontSize: (Dimensions.get('window').width > 320) ? 11 : 9,
        flex: 1,
        textAlign: 'center'
    },
    weekDays: {
        flexDirection: 'row',
        marginBottom: 5
    }
});