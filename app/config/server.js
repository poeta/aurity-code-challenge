/**
 * Created by brunopoeta on 17/08/17.
 */

import axios from 'axios';

// Defining base url for all api calls
axios.defaults.baseURL = 'https://timesheet-training-api.herokuapp.com/api';